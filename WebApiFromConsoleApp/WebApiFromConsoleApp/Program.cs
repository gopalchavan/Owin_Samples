﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiFromConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseUrl = "http://localhost:3000";
            WebApp.Start<Startup>(baseUrl);
            Console.ReadKey();
        }
    }
}
