﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiFromConsoleApp.Models;

namespace WebApiFromConsoleApp.Controllers
{
    public class StudentsController : ApiController
    {
        private static List<Student> _Db = new List<Student> {
            new Student {ID = 1, Name = "Megha" },
            new Student {ID = 2, Name = "Vikas"},
            new Student {ID = 3, Name = "Ranjeet"},
        };
        [Route("api/students")]
        [Authorize(Roles ="Admin")]
        public IEnumerable<Student> GetAllStudents()
        {
            return _Db;
        }
        
    }
}
