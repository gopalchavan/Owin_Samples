﻿using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwinSimpleDemo
{
    using Microsoft.Owin;
    using AppFunc = Func<IDictionary<string, object>, Task>;
    class Program
    {
        static void Main(string[] args)
        {
            // WebApp from owin.hosting is used to start the server.
            WebApp.Start<StartUp>("http://localhost:4001");
            Console.WriteLine("server started at 4001");
            Console.ReadKey();
        }
    }
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (ctx, next) =>
            {
                ctx.Response.WriteAsync("<html><body>");
                await next();
                ctx.Response.WriteAsync("</body></html>");
            });
            ConfigurationOptions options = new ConfigurationOptions("Hello","jo");
            options.IncludeDate = true;
            // Generic way of adding middle were
            app.Use<MyMiddleWere>(options);
            app.Use<MyMiddleWereTwo>();

            // normally adding middle were
            app.Use(async(ctx,next) =>
            {
                ctx.Response.WriteAsync("<p>hello this is the arrow func middleware</p>");
                await next();
            });
        }        
    }
    public class MyMiddleWere
    {
        private AppFunc _next;
        private ConfigurationOptions _greeting;
        public MyMiddleWere(AppFunc next,ConfigurationOptions greeting)
        {
            _next = next;
            _greeting = greeting;
        }
        public async Task Invoke(IDictionary<string,object> environment)
        {
            IOwinContext ctx = new OwinContext(environment);
            ctx.Response.WriteAsync(_greeting.GetGreeting());
            _next(environment);
        }
    }
    public class MyMiddleWereTwo
    {
        private AppFunc _next;

        public MyMiddleWereTwo(AppFunc next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string,object> environment)
        {
            IOwinContext ctx = new OwinContext(environment);
            ctx.Response.WriteAsync("<p>hello this is my middle were two</p>");
            _next(environment);
        }

    }

    public class ConfigurationOptions
    {
        public ConfigurationOptions(string GreetingText,string Greeter)
        {
            this.GreetingText = GreetingText;
            this.Greeter = Greeter;
            Date = DateTime.Now;
        }
        public string GreetingText { get; set; }
        public string Greeter { get; set; }
        public DateTime Date { get; set; }
        public bool IncludeDate { get; set; }

        public string GetGreeting()
        {
            string date="";
            if (IncludeDate)
            {
                date = this.Date.ToShortDateString();
            }
            return "<h1>" + GreetingText + " from " + this.Greeter + " " + date + "</h1>";
        }
    }
}
