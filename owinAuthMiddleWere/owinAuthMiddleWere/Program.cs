﻿using Microsoft.Owin.Hosting;
using Owin;
using owinAuthMiddleWere.AuthMiddlewere;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace owinAuthMiddleWere
{
    class Program
    {
        static void Main(string[] args)
        {
            WebApp.Start<StartUp>("http://localhost:4280");
            Console.WriteLine("Server started at 4280 port");
            Console.ReadLine();
        }
    }
    class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use<LoginMiddleWere>();
            app.Use<AuthinticationMiddleWere>();
        }
    }
}
