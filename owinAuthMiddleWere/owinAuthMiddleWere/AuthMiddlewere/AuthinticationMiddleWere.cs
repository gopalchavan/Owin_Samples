﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace owinAuthMiddleWere.AuthMiddlewere
{
    using Microsoft.Owin;
    using AppFunc = Func<IDictionary<string, object>, Task>;
    class AuthinticationMiddleWere
    {
        private AppFunc _next;
        public AuthinticationMiddleWere(AppFunc next)
        {
            _next = next;
        }   
        
        public async Task Invoke(IDictionary<string,object> environment)
        {
            IOwinContext ctx = new OwinContext(environment);
            bool Authinticated = (ctx.Request.QueryString.Value == "test");
            if (Authinticated)
            {
                ctx.Response.StatusCode = 200;
                ctx.Response.ReasonPhrase = "Successfully Authenticated";
                //_next(environment);
            }
            else
            {
                ctx.Response.StatusCode = 401;
                ctx.Response.ReasonPhrase = "unAuthorize";
            }
        }     
    }
    class LoginMiddleWere
    {
        private AppFunc _next;
        public LoginMiddleWere(AppFunc next)
        {
            _next = next;
        }
        public async Task Invoke(IDictionary<string,object> environment)
        {
            _next(environment);
            IOwinContext ctx = new OwinContext(environment);
            await ctx.Response.WriteAsync(string.Format("State : {0} \n {1}",ctx.Response.StatusCode,ctx.Response.ReasonPhrase));
            
        }
    }
}
